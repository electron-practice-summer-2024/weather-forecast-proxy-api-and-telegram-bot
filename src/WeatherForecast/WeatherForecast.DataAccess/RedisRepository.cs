﻿using Redis.OM.Searching;
using System;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Newtonsoft.Json;
using WeatherForecast.Core.Abstraction;
using WeatherForecast.Core.Domain;

namespace test_web_api
{
    public class RedisRepository: IRepository
    {
        private readonly RedisContext _context;

        public RedisRepository(RedisContext context)
        {
            _context = context;
        }

        public async Task<string> Read(string city, DateOnly date)
        {
            var cityToRead = await _context._city.FirstOrDefaultAsync(x => x.Name == city && x.DateForecast == date);

            if (cityToRead != null)
            {
                return JsonConvert.SerializeObject(cityToRead.Forecast);
            }
            return "NoC";

        }

        public async void AddCity(City city)
        {
            await _context._city.InsertAsync(city);
        }
    }
}
