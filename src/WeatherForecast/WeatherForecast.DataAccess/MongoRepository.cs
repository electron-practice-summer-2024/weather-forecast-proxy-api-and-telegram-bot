﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Core.Domain;
using Newtonsoft.Json;
using WeatherForecast.Core.Abstraction;

namespace WeatherForecast.DataAccess
{
    public class MongoRepository: IRepository
    {
        private readonly IMongoCollection<City> _cityCollection;

        public MongoRepository(MongoContext context)
        {
            _cityCollection = context.WeatherForecast;
        }

        public async Task<string> Read(string city, DateOnly date)
        {
            var cityToRead = await _cityCollection.Find(c => c.Name == city && c.DateForecast == date).FirstOrDefaultAsync();

            if (cityToRead != null)
            {
                return JsonConvert.SerializeObject(cityToRead.Forecast);
            }
            return "NoC";
        }

        public async void AddCity(City city)
        {
            await _cityCollection.InsertOneAsync(city);
        }

    }
}
