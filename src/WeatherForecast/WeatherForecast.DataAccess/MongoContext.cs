﻿using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Core.Domain;

namespace WeatherForecast.DataAccess
{
    public class MongoContext
    {
        private readonly IMongoDatabase _database;
        public readonly IMongoCollection<City> WeatherForecast;

        public MongoContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
            WeatherForecast = _database.GetCollection<City>("weather_forecast");
        }
    }

    public class DateOnlySerializer : SerializerBase<DateOnly>
    {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, DateOnly value)
        {
            context.Writer.WriteString(value.ToString("yyyy-MM-dd"));
        }

        public override DateOnly Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var dateString = context.Reader.ReadString();
            return DateOnly.Parse(dateString);
        }
    }

}
