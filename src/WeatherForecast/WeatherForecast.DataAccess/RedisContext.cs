﻿using MongoDB.Driver.Core.Connections;
using Redis.OM;
using Redis.OM.Contracts;
using Redis.OM.Searching;
using WeatherForecast.Core.Domain;
using System;

namespace test_web_api
{
    public class RedisContext
    {
        private readonly RedisConnectionProvider _provider;
        public readonly RedisCollection<City> _city;
        private readonly IRedisConnection _connection;


        public RedisContext(string connectionString)
        {
            _provider = new RedisConnectionProvider(connectionString);
            _connection = _provider.Connection;
            _city = (RedisCollection<City>)_provider.RedisCollection<City>();
            _connection.CreateIndex(typeof(City));
        }
    }
        
}

