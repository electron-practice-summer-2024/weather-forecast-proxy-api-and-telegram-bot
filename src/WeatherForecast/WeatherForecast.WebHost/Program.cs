
using Microsoft.Extensions.Hosting;
using MongoDB.Bson.Serialization;
using test_web_api;
using WeatherForecast.Core.Abstraction;
using WeatherForecast.DataAccess;
using WeatherForecast.WebHost.Controllers;

using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;

namespace WeatherForecast.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddEnvironmentVariables();

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddTransient<WeatherService>();
            builder.Services.AddTransient<IRepository>(sp => new RedisRepository(new RedisContext("redis://localhost:6379")));
            //builder.Services.AddTransient<IRepository>(sp => new MongoRepository(new MongoContext("mongodb://localhost:27023", "practice")));

            BsonSerializer.RegisterSerializer(new DateOnlySerializer());

            builder.Services.AddScoped<BotController>();
            var app = builder.Build();



            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}