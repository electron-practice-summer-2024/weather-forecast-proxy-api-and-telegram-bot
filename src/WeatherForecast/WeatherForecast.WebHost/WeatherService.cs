﻿using System.Net.Http.Headers;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using static System.Runtime.InteropServices.JavaScript.JSType;
using WeatherForecast.Core.Domain;
using WeatherForecast.Core.Abstraction;

namespace test_web_api
{
    public class WeatherService
    {
        private readonly IRepository db;
        public WeatherService(IRepository _db)
        {
            db = _db;
        }
        public async Task<List<string>> GetFromAPICoder(string city_name)
        {
            string apiUrl = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
            string apiKey = "e163d311efea94faa47bc64cda6bd4b46e4a5eae";
            string jsonData = "{ \"query\": \"" + city_name + "\" }";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", apiKey);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync(apiUrl, content);

                if (response.IsSuccessStatusCode)
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    dynamic dynJson = JsonConvert.DeserializeObject(responseContent);
                    if (dynJson.suggestions.Count == 0)
                    {
                        return new List<string>();
                    }
                    else
                    {
                        string city__name = dynJson.suggestions[0].value.ToString().Substring(2);
                        double geo_latt = dynJson.suggestions[0].data.geo_lat;
                        double geo_lonn = dynJson.suggestions[0].data.geo_lon;
                        var result = new List<string>() { city__name, geo_latt.ToString(), geo_lonn.ToString() };
                        return result;
                    }

                }
                else
                {
                    return new List<string>();
                }
            }
        }
        public async Task<ForecastByHour[]> GetFromAPIWeather(string geo_lat, string geo_lon, DateOnly date)
        {
            var geo_latt = geo_lat.Replace(",", ".");
            var geo_lonn = geo_lon.Replace(",", ".");
            var req = $"https://api.open-meteo.com/v1/forecast?latitude={geo_latt}&longitude={geo_lonn}&variables=temperature,precipitation&hourly=temperature_2m&hourly=pressure_msl&hourly=precipitation";
            var request = WebRequest.Create(req);
            var response = (HttpWebResponse)request.GetResponse();
            var text = "";
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            dynamic dynJson = JsonConvert.DeserializeObject(text);
            DateOnly current_date = DateOnly.FromDateTime(DateTime.Now);
            int comparison = date.DayNumber - current_date.DayNumber;
            ForecastByHour[] weather = new ForecastByHour[24];
            for (int i = 0; i < weather.Length; i++)
            {
                weather[i] = new ForecastByHour();
            }
            int ind = 0;
            for (int i = 24 * comparison; i < 24 + 24 * comparison; i++)
            {
                weather[ind].Temperature = dynJson.hourly.temperature_2m[i];
                weather[ind].Pressure = dynJson.hourly.pressure_msl[i];
                weather[ind].Precipitation = dynJson.hourly.precipitation[i];
                ind++;
            }
            return weather;
        }
        public async Task<string> NewCity(string city_name, DateOnly dat)
        {
            var geo_code = await GetFromAPICoder(city_name);
            if (geo_code.Count != 0)
            {
                var result = await db.Read(geo_code[0], dat);
                if (result != "NoC")
                {
                    return result;
                }
                DateOnly current_date = DateOnly.FromDateTime(DateTime.Now);
                if (dat< current_date)
                {
                    return "Нет информации о погоде на указанный день";
                }
                var weather = await GetFromAPIWeather(geo_code[1], geo_code[2], dat);
                
                City newcity = new City
                {
                    Name = geo_code[0],
                    GeoLat = Convert.ToDouble(geo_code[1]),
                    GeoLon = Convert.ToDouble(geo_code[2]),
                    DateForecast = dat,
                    DateForecastRequest = current_date,
                    Forecast = weather,
                };
                db.AddCity(newcity);
                return JsonConvert.SerializeObject(weather);
            }
            return "Некорректное название города";        
        }
    }
}
