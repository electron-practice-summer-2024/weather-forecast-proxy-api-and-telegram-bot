using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;
using test_web_api;
using WeatherForecast.Core.Domain;
using WeatherForecast.Core.Abstraction;
using WeatherForecast.WebHost;
using WeatherForecast.DataAccess;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Polling;

namespace WeatherForecast.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DBController : ControllerBase
    {
        private readonly IRepository db;
        private readonly ILogger<DBController> _logger;
        private readonly WeatherService weatherService;

        public DBController(ILogger<DBController> logger, IRepository _db, WeatherService ws)
        {
            db = _db;
            weatherService = ws;
            _logger = logger;
        }

        [HttpGet(Name = "GetWeather")]
        public async Task<string> GetWeather(string city_name, string date)
        {
            _logger.LogInformation("LogInformation. ������� ���������: " + city_name + date);
            try
            {
                DateOnly dat = DateOnly.Parse(date);
                var result = await db.Read(city_name, dat);
                if (result == "NoC")
                {
                    var serv = await weatherService.NewCity(city_name, dat);
                    return serv;
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogInformation("LogInformation.������: " + ex.Message);
                return "������������ ������";
            }
        }

    }

   
    
}