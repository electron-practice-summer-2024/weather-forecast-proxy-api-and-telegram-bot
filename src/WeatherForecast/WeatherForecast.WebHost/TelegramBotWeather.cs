﻿using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types;
using Telegram.Bot;
using test_web_api;
using WeatherForecast.Core.Abstraction;
using WeatherForecast.DataAccess;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;
using WeatherForecast.WebHost.Controllers;
using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Net.Http;

namespace WeatherForecast.WebHost
{
    [ApiController]
    [Route("[controller]")]
    public class BotController : ControllerBase
    {
        private readonly IRepository db;
        private readonly ILogger<BotController> _logger;
        private readonly WeatherService weatherService;
        private readonly TelegramBotClient _botClient;
        private readonly DBController dbc;

        private string? _city;
        private string? _date;

        public BotController(ILogger<BotController> logger, IRepository _db, WeatherService ws, IConfiguration configuration, ILogger<DBController> loggerDB)
        {
            db = _db;
            weatherService = ws;
            _logger = logger;
            dbc = new DBController(loggerDB, _db, ws);

            var botToken = configuration.GetValue<string>("BotToken");
            _botClient = new TelegramBotClient(botToken);

            StartReceivingUpdates().ConfigureAwait(true);
        }


        private async Task StartReceivingUpdates()
        {
            var receiverOptions = new ReceiverOptions { AllowedUpdates = { } };
            using var cts = new CancellationTokenSource();
            await Task.Run(() => _botClient.StartReceiving(
                HandleUpdateAsync,
                HandlePollingErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token
            ));
        }

        private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type == UpdateType.Message && update.Message.Type == MessageType.Text)
            {
                var message = update.Message.Text;
                var chatId = update.Message.Chat.Id;

                if (message.StartsWith("/start"))
                {
                    await botClient.SendTextMessageAsync(chatId, "Привет! Я бот, который может показать погоду. Введите город:");
                    return;
                }

                if (_city == null)
                {
                    if (!string.IsNullOrEmpty(message) && message.All(char.IsLetter))
                    {
                        _city = message;
                        await botClient.SendTextMessageAsync(chatId, "Введите дату (формат: yyyy-MM-dd):");
                        return;
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(chatId, "Неверный формат города. Введите заново:");
                        return;
                    }
                }

                if (_date == null)
                {
                    if (DateTime.TryParse(message, out DateTime dt))
                    {
                        _date = message;
                        //var apiUrl = $"http://localhost:5071/DB?city_name={_city}&date={_date}";
                        //var req = "";
                        //var request = WebRequest.Create(req);
                        //var response = (HttpWebResponse)request.GetResponse();
                        //var text = "";
                        //using (var sr = new StreamReader(response.GetResponseStream()))
                        //{
                        //    text = sr.ReadToEnd();
                        //}
                        var result = await dbc.GetWeather(_city, _date);
                        await botClient.SendTextMessageAsync(chatId, result);
                        _city = null;
                        _date = null;
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(chatId, "Неверный формат даты. Введите дату в формате yyyy-MM-dd:");
                        return;
                    }
                }
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Update update)
        {
            if (update.Type == UpdateType.Message && update.Message.Type == MessageType.Text)
            {
                var message = update.Message.Text;

                if (message.StartsWith("/start"))
                {
                    await _botClient.SendTextMessageAsync(update.Message.Chat.Id, "Привет! Я бот на C#.",
                        replyMarkup: new ReplyKeyboardMarkup(new[] {
                            new KeyboardButton("/start")
                        }));
                }
            }
            return Ok();
        }


        private Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Ошибка при получении обновления: {exception.Message}");
            return Task.CompletedTask;
        }
    }
}
