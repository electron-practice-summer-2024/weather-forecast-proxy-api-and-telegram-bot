﻿using Redis.OM.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WeatherForecast.Core.Domain
{
    [Document(StorageType = StorageType.Json, Prefixes = new[] { "City" })]
    public class City:BaseEntity
    {
        [Indexed]
        public string? Name { get; set; }
        [Indexed]
        public DateOnly? DateForecastRequest { get; set; }
        [Indexed]
        public DateOnly? DateForecast { get; set; }
        [Indexed]
        public double GeoLat { get; set; }
        [Indexed]
        public double GeoLon { get; set; }
        [Indexed]
        public ForecastByHour[]? Forecast { get; set; }= Array.Empty<ForecastByHour>();

    }
}
