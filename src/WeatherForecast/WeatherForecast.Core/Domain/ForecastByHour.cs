﻿using Redis.OM.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Core.Domain
{
    public class ForecastByHour
    {
        [Indexed]
        public double? Temperature { get; set; }
        [Indexed]
        public double? Precipitation { get; set; }
        [Indexed]
        public double? Pressure { get; set; }

    }
}
