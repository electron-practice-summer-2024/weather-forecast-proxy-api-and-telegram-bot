﻿using Redis.OM.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace WeatherForecast.Core.Domain
{
    public class BaseEntity
    {
        [RedisIdField][Indexed][BsonId] public string? Id { get; set; } = Guid.NewGuid().ToString();
    }
}
