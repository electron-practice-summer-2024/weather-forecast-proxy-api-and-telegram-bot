﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Core.Domain;

namespace WeatherForecast.Core.Abstraction
{
    public interface IRepository
    {
        void AddCity(City entity);
        Task<string> Read(string city, DateOnly date);
    }
}
