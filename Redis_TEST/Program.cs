﻿using System.Net;
using OpenMeteo;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

// var request = WebRequest.Create("https://api.open-meteo.com/v1/forecast?latitude=55.75&longitude=37.62&variables=temperature,precipitation&hourly=temperature_2m");
// var response = (HttpWebResponse)request.GetResponse();
// var text = "";
//  using (var sr = new StreamReader(response.GetResponseStream()))
// {
//     text = sr.ReadToEnd();
// }
// Console.WriteLine(text);
// var request = WebRequest.Create("https://cleaner.dadata.ru/api/v1/clean/address?key=e163d311efea94faa47bc64cda6bd4b46e4a5eae&query=москва");
// request.GetResponse()
// var response = (HttpWebResponse)request.GetResponse();
// var text = "";
//  using (var sr = new StreamReader(response.GetResponseStream()))
// {
//     text = sr.ReadToEnd();
// }
// Console.WriteLine(text);

// using (var wb = new WebClient())
// {
//     var data = new NameValueCollection();
//     data["query"] = "москва хабар";
//     wb.Headers
//     var response = wb.UploadValues("http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address", "POST", data);
//     string responseInString = Encoding.UTF8.GetString(response);
// }
// var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://url");
// httpWebRequest.ContentType = "application/json";
// httpWebRequest.Method = "POST";
string apiUrl = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
string apiKey = "e163d311efea94faa47bc64cda6bd4b46e4a5eae";
string jsonData = "{ \"query\": \"спб ленинский\" }";

using (HttpClient client = new HttpClient())
{
    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", apiKey);
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

    HttpResponseMessage response = await client.PostAsync(apiUrl, content);

    if (response.IsSuccessStatusCode)
    {
        string responseContent = await response.Content.ReadAsStringAsync();
        Console.WriteLine(responseContent);
    }
    else
    {
        Console.WriteLine($"Ошибка: {response.StatusCode}");
    }
}