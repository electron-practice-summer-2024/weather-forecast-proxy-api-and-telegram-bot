# Weather forecast proxy API and Telegram bot



## Введение
Данное техническое задание распространяется на разработку телеграм-бота для просмотра прогноза погоды.
Телеграм-бот помогает просматривать прогноз погоды по определенному городу и выбранному периоду.

## Основания для разработки
Задание на разработку в рамках производственной практики.

## Назначение разработки 
Целью разработки является создание телеграм-бота для просмотра прогноз погоды по определенному городу.  Бот должен быть легко доступным  для пользователей и предоставлять им возможность ввода исходных данных и вывода прогноза.

## Требования к программе и программному изделию 
### Требования к графическому дизайну бота
Графический интерфейс бота должен быть интуитивно понятным и привлекательным для пользователей. Элементы управления должны быть удобными для использования.

### Требования к функциональным характеристикам
#### Классы пользователей 
Бот должен поддерживать все типы пользователей.

#### Требования к функциональной части 
- Возможность ввода исходных данных пользователем: Город, период прогноза (текущая погода , на 3 дня, на 7 дней)
- Возможность обращения к API геокодера (https://dadata.ru/api/suggest/address/) для получения ширины и долготы по выбранному городу.
- Возможность обращения к API прогноза погоды (https://open-meteo.com/en/docs) для получения информации о погоде.
- Возможность вывода информации пользователю.
- Возможность сохранения информации о погоде в базе данных.

#### Требования к представлению бота
Меню бота должно содержать список с городами для выбора, и список с периодами прогноза.

### Требования к программному обеспечению 
Требования к части разработчиков: язык программирования C# с использованием API прогноза погоды и геокодера, базы данных MongoDB и разработка тестов.
Требования к клиентской части: разработка интуитивно понятного интерфейса, который позволяет пользователям легко вводить исходные данные и просматривать результаты. Интерфейс должен включать элементы управления для выбора города,  периода прогноза и визуализации результатов.